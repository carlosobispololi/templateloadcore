package com.canvia.templateload;

import com.canvia.templateload.bean.TemplateBean;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class EntityMock {

    private static final EntityMock INSTANCE = new EntityMock();

    private static ObjectMapper objectMapper = new ObjectMapper();

    private EntityMock(){

    }

    public static EntityMock getINSTANCE() {
        return INSTANCE;
    }

    public TemplateBean buildTemplateBean() throws IOException {
        return objectMapper.readValue(Thread.currentThread()
                .getContextClassLoader().getResourceAsStream("mock/templeateBean.json"), TemplateBean.class);
    }

}
