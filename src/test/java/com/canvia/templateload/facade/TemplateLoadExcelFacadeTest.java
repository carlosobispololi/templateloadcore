package com.canvia.templateload.facade;

import com.canvia.templateload.facade.impl.TemplateLoadExcelFacade;
import org.junit.Before;
import org.junit.Test;

/**
 * created on 24/05/2019
 *
 * @author Canvia
 */
public class TemplateLoadExcelFacadeTest {

    ITemplateLoadFacade templateLoadFacade;

    @Before
    public void setUp(){
        templateLoadFacade = new TemplateLoadExcelFacade();
    }

    @Test
    public void processFileTest(){
        templateLoadFacade.processFile();
    }


}
