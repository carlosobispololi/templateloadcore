package com.canvia.templateload.motorcorreo;

import com.canvia.templateload.EntityMock;
import com.canvia.templateload.bean.TemplateBean;
import org.junit.Before;
import org.junit.Test;

/**
 * created on 23/05/2019
 *
 * @author Canvia
 */
public class NewBeginningProcessTest {

    private NewBeginningProcess newBeginningProcess;

    @Before
    public void setUp(){
        newBeginningProcess = new NewBeginningProcess();
    }

    public void processImagesTest() throws Exception {
        TemplateBean templateBean = EntityMock.getINSTANCE().buildTemplateBean();
        newBeginningProcess.processImages(templateBean);
    }

    @Test
    public void processParametersTest() throws Exception {
        TemplateBean templateBean = EntityMock.getINSTANCE().buildTemplateBean();
        newBeginningProcess.processParameters(templateBean);
    }

}
