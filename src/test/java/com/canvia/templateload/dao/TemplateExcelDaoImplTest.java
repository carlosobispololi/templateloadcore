package com.canvia.templateload.dao;

import com.canvia.templateload.bean.TemplateBean;
import com.canvia.templateload.dao.impl.TemplateExcelDaoImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;


/**
 * created on 22/05/2019
 *
 * @author Canvia
 */
public class TemplateExcelDaoImplTest {

    private ITemplateDao templateDao;

    @Before
    public void setUp(){
        templateDao = new TemplateExcelDaoImpl();
    }

    @Test
    public void mapInTest() throws Exception{

        InputStream inputStream = Thread.currentThread()
                .getContextClassLoader().getResourceAsStream("mock/Solicitud Plantilla.xlsx");

        TemplateBean templateBean = templateDao.buildTemplateBean(inputStream);
        Assert.assertNotNull(templateBean);
        Assert.assertNotNull(templateBean.getCode());
        Assert.assertNotNull(templateBean.getPageNumber());
        Assert.assertNotNull(templateBean.getParameters());
        Assert.assertNotNull(templateBean.getImages());
    }
}
