package com.canvia.templateload.service;

import com.canvia.templateload.bean.TemplateBean;

public interface ITemplateService {

    TemplateBean getTemplateBean(String pathExcelTemplate) throws Exception;

    
}
