package com.canvia.templateload.service.impl;

import com.canvia.templateload.bean.TemplateBean;
import com.canvia.templateload.dao.ITemplateDao;
import com.canvia.templateload.dao.factory.FactoryDao;
import com.canvia.templateload.service.ITemplateService;

import java.io.FileInputStream;

public class TemplateServiceImpl implements ITemplateService {

    private FactoryDao factoryDao = FactoryDao.getFactory(FactoryDao.EXCEL_FACTORY);
    private ITemplateDao templateExcelDao = factoryDao.getPlantillaDao();


    public TemplateBean getTemplateBean(String pathExcelTemplate) throws Exception {
        FileInputStream fileInputStream = new FileInputStream( pathExcelTemplate);
        return templateExcelDao.buildTemplateBean(fileInputStream);
    }
}
