package com.canvia.templateload.util.bundle;

import com.canvia.templateload.constant.ApplicationConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.ResourceBundle;

public class ResourceBundleUtil implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(ResourceBundleUtil.class);

    private static final long serialVersionUID = 1L;
    private static Properties properties;
    private static ResourceBundle resourceBundle;

    static{
        try {
            File file = new File(ApplicationConstant.CONFIG_RESOURCE_EXTERNAL_BUNDLE);
            if( file.exists()){
                InputStream inStream = new FileInputStream(file);
                properties= new Properties();
                properties.load(inStream);
            }else{
                resourceBundle = ResourceBundle.getBundle(ApplicationConstant.CONFIG_RESOURCE_BUNDLE);
            }
        }catch (Exception e){
            LOGGER.error("ERROR al cargar properties ",e);
        }
    }

    public static String getProperty(String key) {
        String result;
        try {
            if(properties!=null){
                result = properties.getProperty(key);
            }else{
                result = resourceBundle.getString(key);
            }
        } catch (MissingResourceException e) {
            LOGGER.error(String.format("Property key '%s' is not defined", key));
            return "";
        }
        return result;
    }

}
