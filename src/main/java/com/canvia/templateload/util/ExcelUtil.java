package com.canvia.templateload.util;

import com.canvia.templateload.constant.ExcelContants;
import org.apache.poi.hssf.util.CellReference;
import org.apache.poi.ss.usermodel.Cell;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

/**
 * created on 22/05/2019
 *
 * @author Canvia
 */
public class ExcelUtil{

    private static final Logger LOGGER = LoggerFactory.getLogger(ExcelUtil.class);

    private static  ExcelUtil INSTANCE;
    private XSSFSheet xssfSheet;

    private ExcelUtil(InputStream SheetProcess) throws IOException {
        LOGGER.info("...invoke ExcelUtil.initializeExcel ...");
        xssfSheet = new XSSFWorkbook(SheetProcess)
                .getSheetAt(ExcelContants.NUMBER_SHEET_PROCESS);
    }

    public static ExcelUtil getINSTANCE(InputStream sheetProcess) throws IOException {
        if(INSTANCE ==null){
            INSTANCE=  new ExcelUtil(sheetProcess);
        }
        return INSTANCE;
    }

    public Iterator<Row> getExcelRows(){
        return xssfSheet.iterator();
    }

    public String getStringCellValue( String ubicationCell ){
        LOGGER.info("...invoke ExcelUtil.getValueCell ... ubicationCell: " + ubicationCell);
        CellReference cellReference = new CellReference( ubicationCell);
        Row rowExcel = xssfSheet.getRow(cellReference.getRow());
        if ( rowExcel != null) {
            Cell cell = rowExcel.getCell( cellReference.getCol() );
            LOGGER.debug( "cell.getCellTypeEnum: " + cell.getCellTypeEnum() );
            return getCellValue(cell);
        }
        return "";
    }

    public String getCellValue(Cell cell){
        switch ( cell.getCellTypeEnum() ){
            case STRING:
                return cell.getStringCellValue();
            case NUMERIC:
                return cell.getNumericCellValue() + "";
            case BOOLEAN:
                return cell.getBooleanCellValue() + "";
             default:
                 return "";
        }
    }

}
