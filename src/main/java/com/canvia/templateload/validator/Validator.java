package com.canvia.templateload.validator;

public interface Validator<T> {

    boolean validate(T t);
}
