package com.canvia.templateload.validator.impl;

import com.canvia.templateload.bean.ParameterBean;
import com.canvia.templateload.validator.Validator;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ParameterValidator implements Validator<ParameterBean> {

    private static final Logger LOGGER= LoggerFactory.getLogger( ParameterValidator.class);

    @Override
    public boolean validate(ParameterBean parameterBean) {
        LOGGER.info("...invoke ImageValidator.validate ...");
        if( parameterBean==null ){
            LOGGER.error("parameterBean null");
            return Boolean.FALSE;
        }else if(StringUtils.isEmpty(parameterBean.getFieldCode())){
            LOGGER.error("parameterBean.code empty");
            return Boolean.FALSE;
        }else if(StringUtils.isEmpty(parameterBean.getSourceCode())){
            LOGGER.error("parameterBean.sourceCode empty");
            return Boolean.FALSE;
        }else if(StringUtils.isEmpty(parameterBean.getDescription())){
            LOGGER.error("parameterBean.description empty");
            return Boolean.FALSE;
        }else if(StringUtils.isEmpty(parameterBean.getIndTable())){
            LOGGER.error("parameterBean.indTable empty");
            return Boolean.FALSE;
        }else if( parameterBean.getPostInitial()==null){
            LOGGER.error("parameterBean.postInitial empty");
            return Boolean.FALSE;
        }else if( parameterBean.getLength()==null){
            LOGGER.error("parameterBean.length empty");
            return Boolean.FALSE;
        }else if(StringUtils.isEmpty(parameterBean.getTypeField())){
            LOGGER.error("parameterBean.TypeField empty");
            return Boolean.FALSE;
        }else if(StringUtils.isEmpty(parameterBean.getMantEspaces())){
            LOGGER.error("parameterBean.TypeField empty");
            return Boolean.FALSE;
        }else if(StringUtils.isEmpty(parameterBean.getAction())){
            LOGGER.error("parameterBean.action empty");
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }


}
