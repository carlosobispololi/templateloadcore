package com.canvia.templateload.validator.impl;

import com.canvia.templateload.bean.TemplateBean;
import com.canvia.templateload.validator.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TemplateValidator implements Validator<TemplateBean> {

    private static final Logger LOGGER= LoggerFactory.getLogger( TemplateValidator.class);

    public boolean validate(TemplateBean templateBean) {
        LOGGER.info("...invoke TemplateValidator.validate ...");
        if(templateBean==null){
            LOGGER.error("templateBean null");
            return Boolean.FALSE;
        }else if( templateBean.getCode()==null || "".equals(templateBean.getCode()) ){
            LOGGER.error("templateBean.code empty");
            return Boolean.FALSE;
        }else if( templateBean.getPageNumber()==null ){
            LOGGER.error("templateBean.pageNumber empty");
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }
}
