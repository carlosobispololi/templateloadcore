package com.canvia.templateload.validator.impl;

import com.canvia.templateload.bean.ImageBean;
import com.canvia.templateload.validator.Validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ImageValidator implements Validator<ImageBean> {

    private static final Logger LOGGER= LoggerFactory.getLogger(ImageValidator.class);

    public boolean validate(ImageBean imageBean) {
        LOGGER.info("...invoke ImageValidator.validate ...");
        if( imageBean==null ){
            LOGGER.error("imageBean null");
            return Boolean.FALSE;
        }else if( imageBean.getCode()==null || "".equals(imageBean.getCode())){
            LOGGER.error("imageBean.code empty");
            return Boolean.FALSE;
        }else if( imageBean.getName()==null || "".equals(imageBean.getName())){
            LOGGER.error("imageBean.name empty");
            return Boolean.FALSE;
        }else if( imageBean.getAction()==null || "".equals(imageBean.getAction())){
            LOGGER.error("imageBean.action empty");
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }
}
