package com.canvia.templateload.motorcorreo.page;

import com.canvia.templateload.bean.TemplateBean;
import com.canvia.templateload.constant.ApplicationConstant;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class TemplateMaintenancePage {

    private final WebDriver driver;

    public TemplateMaintenancePage(WebDriver driver){
        this.driver = driver;
    }

    public void gotoPlantillaPage(TemplateBean templateBean){
        Integer pageKey = templateBean.getPageNumber();
        String urlPage=  ApplicationConstant.URL_TEMPLATE_PAGINATION_PAGE + ( pageKey - 1);

        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.get(urlPage);
    }

    public List<WebElement> getRowTablePlantillaPage(){
        return driver.findElements(By.cssSelector("table#table tr"));
    }

    public void selectActionEditImages(int numFila){
        driver.findElement(By.cssSelector("#table > tbody > tr:nth-child("+numFila+") >" +
                " td:nth-child(11) > a:nth-child(2)") ).click();
    }

    public void selectActionEditParameters(int numFila){
        driver.findElement(By.cssSelector("#table > tbody > tr:nth-child("+numFila+") >" +
                " td:nth-child(11) > a:nth-child(3)") ).click();
    }

}
