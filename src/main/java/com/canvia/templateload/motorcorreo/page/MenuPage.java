package com.canvia.templateload.motorcorreo.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class MenuPage {

    private final WebDriver driver;

    public MenuPage(WebDriver driver){
        this.driver = driver;
    }

    public void clickLinkMantenimientoPlantillas(){
        driver.findElement(By.cssSelector("#dmenu > li:nth-child(1) > a") ).click();
    }


}
