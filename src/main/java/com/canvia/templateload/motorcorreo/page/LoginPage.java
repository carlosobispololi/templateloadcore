package com.canvia.templateload.motorcorreo.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage {

    private final WebDriver driver;
    private final String url;

    public LoginPage(WebDriver driver, String url) {
        super();
        this.driver = driver;
        this.url = url;
    }

    public void gotoLoginPage() {
        driver.get(url);
    }

    public void loginUser(String usuario) {
        driver.findElement(By.id("formLogin_Usuario")).sendKeys(usuario);
    }

    public void loginPassword(String password) {
        driver.findElement(By.id("formLogin_Password")).sendKeys(password);
    }

    public void clickButtonLogin() {
        driver.findElement(By.id("formLogin_LOGIN") ).click();
    }

    public void driverQuit(){
        driver.quit();
    }

}