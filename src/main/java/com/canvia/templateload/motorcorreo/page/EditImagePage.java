package com.canvia.templateload.motorcorreo.page;

import com.canvia.templateload.constant.ApplicationConstant;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class EditImagePage {

    private final WebDriver driver;

    public EditImagePage(WebDriver driver){
        this.driver = driver;
    }

    public void gotoImagenPage(String paginationNumber){
        Integer pagination = Integer.valueOf(paginationNumber);
        String urlPage=  ApplicationConstant.URL_IMAGE_PAGINATION_PAGE + ( pagination - 1);
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.get(urlPage);
    }

    public WebElement getRowPaginationImagenPage(){
        return driver.findElement(By.cssSelector("#table > tbody > tr.paging-inline > td"));
    }

    public List<WebElement> getRowsTableImagenPage(){
        return driver.findElements(By.cssSelector("table#table tr"));
    }

    public String getTemplateCodeSesion(){
        return driver.findElement(By.xpath("//*[@id=\"formTitulo-fields\"]/tbody/tr/td/div/div[1]")).getText();
    }

    public void selectOptionEditImage(int numFila){
        driver.findElement(By.cssSelector("#table > tbody > tr:nth-child("+numFila+") > td:nth-child(3) > a") ).click();
    }

    public void selectOptionDeleteImage(int numFila){
        driver.findElement(By.cssSelector("#table > tbody > tr:nth-child("+numFila+") > td:nth-child(4) > a") ).click();
    }

    public void confirmDelete(){
        driver.switchTo().alert().accept();
    }

    public void loadImage(String imagen_cargar){
        WebElement uploadElement = driver.findElement(By.id("formImagen_mime"));
        uploadElement.sendKeys(imagen_cargar );
    }

    public void selectButtonSave(){
        driver.findElement(By.id("formImagen_ok") ).click();
    }

    public void selectButtonNewImage(){
        driver.findElement(By.id("form_save") ).click();
    }

    public void writeImageCode(String imageCode) {
        driver.findElement(By.id("formImagen_codigo")).clear();
        driver.findElement(By.id("formImagen_codigo")).sendKeys(imageCode);
    }



}
