package com.canvia.templateload.motorcorreo.page;

import com.canvia.templateload.bean.ParameterBean;
import com.canvia.templateload.constant.ApplicationConstant;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class EditParameterPage {


    private final WebDriver driver;

    public EditParameterPage(WebDriver driver){
        this.driver=driver;
    }

    public void gotoParameterPage(String paginationNumber){
        Integer pagination = Integer.valueOf(paginationNumber);
        String urlPage=  ApplicationConstant.URL_PARAMETER_PAGINATION_PAGE + ( pagination - 1);
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.get(urlPage);
    }

    public WebElement getRowPaginationParameterPage(){
        return driver.findElement(By.cssSelector("#tablaParametro > tbody > tr.paging-inline > td"));
    }


    public List<WebElement> getRowsTableParameterPage(){
        return driver.findElements(By.cssSelector("table#tablaParametro tr"));
    }

    public String getTemplateCodeSesion(){
        return driver.findElement(By.xpath("//*[@id=\"formTitulo-fields\"]/tbody/tr/td/font")).getText();
    }

    public String getTemplateCodeSessionPageNew(){
        return driver.findElement(By.xpath("//*[@id=\"form-fields\"]/tbody/tr[1]/td/font")).getText();
    }

    public void selectOptionEditParameter(int numFila){
        driver.findElement(By.cssSelector("#tablaParametro > tbody > tr:nth-child("+numFila+") > " +
                "td:nth-child(11) > a:nth-child(1)") ).click();
    }

    public void selectOptionDeleteParameter(int numFila){
        driver.findElement(By.cssSelector("#tablaParametro > tbody > tr:nth-child("+numFila+") > " +
                "td:nth-child(11) > a:nth-child(2)") ).click();
    }

    public void confirmDelete(){
        driver.switchTo().alert().accept();
    }

    public void selectButtonNewParameter(){
        driver.findElement(By.id("formParametro_save") ).click();
    }

    public void writeParameter(ParameterBean parameterBean) {
        driver.findElement(By.id("form_campoPlantilla")).clear();
        driver.findElement(By.id("form_campoPlantilla")).sendKeys(parameterBean.getFieldCode());

        driver.findElement(By.id("form_campoOrigen")).clear();
        driver.findElement(By.id("form_campoOrigen")).sendKeys(parameterBean.getSourceCode());

        driver.findElement(By.id("form_descripcion")).clear();
        driver.findElement(By.id("form_descripcion")).sendKeys(parameterBean.getDescription());

        driver.findElement(By.id("form_indicadorTabla")).clear();
        driver.findElement(By.id("form_indicadorTabla")).sendKeys(parameterBean.getIndTable());

        driver.findElement(By.id("form_posicionInicial")).clear();
        driver.findElement(By.id("form_posicionInicial")).sendKeys(parameterBean.getPostInitial().toString());

        driver.findElement(By.id("form_longitud")).clear();
        driver.findElement(By.id("form_longitud")).sendKeys(parameterBean.getLength().toString());

        driver.findElement(By.id("form_tipoCampo")).sendKeys(parameterBean.getTypeField());

        final Select selectBox = new Select(driver.findElement(By.id("form_flgEspacio")));
        selectBox.selectByValue(parameterBean.getMantEspaces());
    }

    public void selectButtonSave(){
        driver.findElement(By.id("form_ok") ).click();
    }


}
