package com.canvia.templateload.motorcorreo;

import com.canvia.templateload.bean.ImageBean;
import com.canvia.templateload.bean.ParameterBean;
import com.canvia.templateload.bean.TemplateBean;
import com.canvia.templateload.constant.ApplicationConstant;
import com.canvia.templateload.motorcorreo.page.LoginPage;
import com.canvia.templateload.motorcorreo.page.MenuPage;
import com.canvia.templateload.motorcorreo.page.TemplateMaintenancePage;
import com.canvia.templateload.motorcorreo.process.image.FactoryImage;
import com.canvia.templateload.motorcorreo.process.image.ImageAction;
import com.canvia.templateload.motorcorreo.process.parameter.FactoryParameter;
import com.canvia.templateload.motorcorreo.process.parameter.ParameterAction;
import com.canvia.templateload.validator.Validator;
import com.canvia.templateload.validator.impl.ImageValidator;
import com.canvia.templateload.validator.impl.ParameterValidator;
import org.apache.commons.collections4.CollectionUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class NewBeginningProcess {

    private static final Logger LOGGER = LoggerFactory.getLogger( NewBeginningProcess.class);

    private static WebDriver driver;
    private static LoginPage loginPage;
    private static TemplateMaintenancePage templateMaintenancePage;
    private static MenuPage menuPage;


    private Validator<ImageBean> imageBeanValidator;
    private Validator<ParameterBean> parameterBeanValidator;

    public NewBeginningProcess(){
        System.setProperty("webdriver.chrome.driver", ApplicationConstant.PATH_CHROME_DRIVE);
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS) ;
        loginPage = new LoginPage(driver, ApplicationConstant.LOGIN_PAGE);
        menuPage = new MenuPage(driver);
        templateMaintenancePage = new TemplateMaintenancePage(driver);
        imageBeanValidator = new ImageValidator();
        parameterBeanValidator = new ParameterValidator();
        ingresarSistema();
    }

    public void processImages(TemplateBean templateBean) throws  Exception{
        LOGGER.info("...invoke NewBeginningProcess.processImages ..." + templateBean.getImages().size());
        if( CollectionUtils.isEmpty( templateBean.getImages() ) ){
            LOGGER.warn("templateBean.getImages Empty");
            return;
        }
        menuPage.clickLinkMantenimientoPlantillas();
        templateMaintenancePage.gotoPlantillaPage(templateBean);
        List<WebElement> rowsTablePlantillaPage = templateMaintenancePage.getRowTablePlantillaPage();
        int rowNumberElement = getRowNumberElement(rowsTablePlantillaPage, templateBean.getCode());
        templateMaintenancePage.selectActionEditImages(rowNumberElement);

        for (ImageBean imageBean: templateBean.getImages() ) {
            if(!imageBeanValidator.validate(imageBean)){
                LOGGER.warn("imageBean no valid ");
                continue;
            }
            ImageAction imageAction = FactoryImage
                    .getImageAction(driver,templateBean.getCode(),imageBean);
            boolean result = imageAction.action();
            LOGGER.info("==> Image: " + imageBean.getCode()+ " RESULT: " + result);
        }
    }

    public void processParameters(TemplateBean templateBean) throws Exception{
        LOGGER.info("...invoke NewBeginningProcess.processParameters ..." + templateBean.getParameters().size());
        if( CollectionUtils.isEmpty( templateBean.getParameters()) ){
            LOGGER.warn("templateBean.getParameters Empty");
            return;
        }
        menuPage.clickLinkMantenimientoPlantillas();
        templateMaintenancePage.gotoPlantillaPage(templateBean);
        List<WebElement> rowsTablePlantillaPage = templateMaintenancePage.getRowTablePlantillaPage();
        int rowNumberElement = getRowNumberElement(rowsTablePlantillaPage, templateBean.getCode());
        templateMaintenancePage.selectActionEditParameters(rowNumberElement);

        for(ParameterBean parameterBean: templateBean.getParameters()){
            if(!parameterBeanValidator.validate(parameterBean)){
                LOGGER.warn("parameterBean no valid");
                continue;
            }
            ParameterAction parameterAction= FactoryParameter
                    .getParameterAction(driver,templateBean.getCode(), parameterBean);
            boolean result = parameterAction.action();
            LOGGER.info("==> Parameter: " + parameterBean.getFieldCode()+ " RESULT: " + result);
        }

        loginPage.driverQuit();
    }


    private int getRowNumberElement(List<WebElement> filas, String identificadorFila ) throws Exception{
        boolean existe= false;
        int numFila = 0;
        for(WebElement fila: filas)
        {
            if( fila.getText().contains(identificadorFila) ){
                existe =true;
                break;
            }
            numFila ++;
        }
        if(!existe){
            throw new Exception("Elemento "+identificadorFila+" no se encuentra en la pagina indicada");
        }
        return numFila;
    }


    private void ingresarSistema(){
        loginPage.gotoLoginPage();
        loginPage.loginUser(ApplicationConstant.USER);
        loginPage.loginPassword(ApplicationConstant.PASSWORD);
        loginPage.clickButtonLogin();
    }

}
