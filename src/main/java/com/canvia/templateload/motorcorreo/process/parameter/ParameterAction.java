package com.canvia.templateload.motorcorreo.process.parameter;

public interface ParameterAction {
    boolean action();
}
