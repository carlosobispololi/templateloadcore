package com.canvia.templateload.motorcorreo.process.image.impl;


import com.canvia.templateload.bean.ImageBean;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class CreateImage extends  ProcessImage {

    private static final Logger LOGGER = LoggerFactory.getLogger(CreateImage.class);

    public CreateImage(WebDriver driver, String templateCode, ImageBean imageBean) {
        super(driver, templateCode, imageBean);
    }

    public boolean action() {
        LOGGER.info("...invoke CreateImage.action ...");
        String pathImage = pathImageLoad(imageBean.getName());
        if( "".equals(pathImage)){
            return false;
        }
        List<String> listPaginationNumbers = listPaginationNumbers();
        for ( String paginationNumber:listPaginationNumbers ) {
            editImagePage.gotoImagenPage(paginationNumber);
            List<WebElement> imagesRow= editImagePage.getRowsTableImagenPage();

            for (WebElement imageRow: imagesRow) {
                if( imageRow.getText().contains(imageBean.getCode()) ){
                    LOGGER.error("Codigo de imagen YA REGISTRADO !!!");
                    return false;
                }
            }
        }
        editImagePage.selectButtonNewImage();

        editImagePage.writeImageCode(imageBean.getCode());
        editImagePage.loadImage(pathImage);
        editImagePage.selectButtonSave();
        return true;
    }


}
