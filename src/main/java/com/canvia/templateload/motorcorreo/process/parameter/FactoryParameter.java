package com.canvia.templateload.motorcorreo.process.parameter;

import com.canvia.templateload.bean.ParameterBean;
import com.canvia.templateload.motorcorreo.process.parameter.impl.CreateParameter;
import com.canvia.templateload.motorcorreo.process.parameter.impl.DeleteParameter;
import com.canvia.templateload.motorcorreo.process.parameter.impl.UpdateParameter;
import org.openqa.selenium.WebDriver;

public class FactoryParameter {

    private static final String MODIFICAR_ACTION ="MODIFICAR";
    private static final String DELETE_ACTION ="ELIMINAR";
    private static final String CREATE_ACTION ="NUEVO";

    public static ParameterAction getParameterAction(WebDriver driver,
                                                     String templateCode , ParameterBean parameterBean){
        String action = parameterBean.getAction().toUpperCase();
        if(MODIFICAR_ACTION.equals(action)){
            return new UpdateParameter(driver,templateCode,parameterBean);
        }
        else if(DELETE_ACTION.equals(action)){
            return new DeleteParameter(driver,templateCode,parameterBean);
        }
        else if(CREATE_ACTION.equals(action)){
            return new CreateParameter(driver,templateCode,parameterBean);
        }
        else{
            throw new IllegalArgumentException("Action no defined"
                    + String.format(" <%s>", action));
        }
    }

}
