package com.canvia.templateload.motorcorreo.process.image;

public interface ImageAction {
    boolean action();
}
