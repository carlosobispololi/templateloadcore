package com.canvia.templateload.motorcorreo.process.parameter.impl;

import com.canvia.templateload.bean.ParameterBean;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class CreateParameter extends ProcessParameter{

    private static final Logger LOGGER = LoggerFactory.getLogger( CreateParameter.class);

    public CreateParameter(WebDriver driver, String codeTemplate, ParameterBean parameterBean) {
        super(driver, codeTemplate, parameterBean);
    }

    @Override
    public boolean action() {
        LOGGER.info("...invoke UpdateParameter.action ...");

        List<String> listPaginationNumbers = listPaginationNumbers();
        for ( String paginationNumber:listPaginationNumbers ) {
            editParameterPage.gotoParameterPage(paginationNumber);
            List<WebElement> parametersRow= editParameterPage.getRowsTableParameterPage();

            for (WebElement parameterRow: parametersRow) {

                if( parameterRow.getText().contains(parameterBean.getFieldCode()) ){
                    LOGGER.error("Codigo de parametro YA REGISTRADO !!!");
                    return false;
                }
            }
        }

        editParameterPage.selectButtonNewParameter();
        if(!editParameterPage.getTemplateCodeSessionPageNew().contains(TEMPLATE_CODE)){
            LOGGER.error("Session de plantilla invalida ");
            return false;
        }
        editParameterPage.writeParameter(parameterBean);
        editParameterPage.selectButtonSave();
        return true;

    }
}
