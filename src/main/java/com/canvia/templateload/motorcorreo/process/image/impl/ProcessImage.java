package com.canvia.templateload.motorcorreo.process.image.impl;

import com.canvia.templateload.bean.ImageBean;
import com.canvia.templateload.constant.ApplicationConstant;
import com.canvia.templateload.motorcorreo.page.EditImagePage;
import com.canvia.templateload.motorcorreo.process.image.ImageAction;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public abstract class ProcessImage implements ImageAction {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProcessImage.class);

    protected  static final String NO_RECORD_FOUND = "No records found";
    private static final String FILE_IMAGE = "img";
    protected  EditImagePage editImagePage;
    protected  String TEMPLATE_CODE;
    protected ImageBean imageBean;

    public ProcessImage(WebDriver driver, String codeTemplate,ImageBean imageBean){
        this.editImagePage = new EditImagePage(driver);
        this.TEMPLATE_CODE = codeTemplate;
        this.imageBean = imageBean;
    }

    protected List<String>  listPaginationNumbers(){
        List<String> listPaginationNumbers= new ArrayList<String>();
        WebElement webElementPagination = editImagePage.getRowPaginationImagenPage();
        String[] elementsPagination = webElementPagination.getText()
                .replace(" ", "-").split("-");
        for (String elementPagination : elementsPagination ){
            if(StringUtils.isNumeric(elementPagination)){
                listPaginationNumbers.add(elementPagination);
            }
        }
        return listPaginationNumbers;
    }

    protected String pathImageLoad(String ImageName){
        String pathImage = ApplicationConstant.PATH_PROCESS + "\\" + TEMPLATE_CODE +
                "\\" + FILE_IMAGE + "\\" + ImageName;
        if( !new File(pathImage).exists()){
            LOGGER.error("ERROR,  Elemento a cargar no existe , RUTA : " + pathImage);
            return  "";
        }
        return pathImage;
    }

    protected boolean isvalidSession(){
        String templateCodeSession = editImagePage.getTemplateCodeSesion();
        if( !templateCodeSession.contains(TEMPLATE_CODE)){
            LOGGER.error("ERROR, Codigo de Plantilla en session invalido, " +
                    "<" + templateCodeSession + "> " +
                    ", TEMPLATE_CODE <"+TEMPLATE_CODE+">");
            return false;
        }
        return true;
    }
}
