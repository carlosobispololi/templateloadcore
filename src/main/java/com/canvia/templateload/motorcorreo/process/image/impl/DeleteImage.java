package com.canvia.templateload.motorcorreo.process.image.impl;


import com.canvia.templateload.bean.ImageBean;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class DeleteImage extends ProcessImage {

    private static final Logger LOGGER = LoggerFactory.getLogger(DeleteImage.class);

    public DeleteImage(WebDriver driver, String templateCode, ImageBean imageBean) {
        super(driver, templateCode, imageBean);
    }

    public boolean action() {
        LOGGER.info("...invoke UpdateImage.action ...");

        List<String> listPaginationNumbers = listPaginationNumbers();
        for ( String paginationNumber:listPaginationNumbers ) {
            editImagePage.gotoImagenPage(paginationNumber);
            List<WebElement> imagesRow= editImagePage.getRowsTableImagenPage();

            int rowNumber = 0;
            for (WebElement imageRow: imagesRow) {
                if(imageRow.getText().contains(NO_RECORD_FOUND)){
                    LOGGER.error("Plantilla no tiene imagenes cargadas, " + NO_RECORD_FOUND);
                    return false;
                }
                if( imageRow.getText().contains(imageBean.getCode()) ){
                    if( !isvalidSession()){
                        return false;
                    }
                    editImagePage.selectOptionDeleteImage(rowNumber);
                    editImagePage.confirmDelete();
                    return true;
                }
                rowNumber++;
            }
        }
        LOGGER.error("Imagenen a Eliminar no existe en WAS!!!");
        return false;

    }
}
