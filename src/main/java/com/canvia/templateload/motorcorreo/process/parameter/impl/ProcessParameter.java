package com.canvia.templateload.motorcorreo.process.parameter.impl;

import com.canvia.templateload.bean.ParameterBean;
import com.canvia.templateload.motorcorreo.page.EditParameterPage;
import com.canvia.templateload.motorcorreo.process.parameter.ParameterAction;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public abstract class ProcessParameter implements ParameterAction {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProcessParameter.class);

    protected  static final String NO_RECORD_FOUND = "No records found";
    protected static String TEMPLATE_CODE;
    protected ParameterBean parameterBean;
    protected EditParameterPage editParameterPage;

    public ProcessParameter(WebDriver driver, String codeTemplate, ParameterBean parameterBean){
        this.TEMPLATE_CODE = codeTemplate;
        this.parameterBean = parameterBean;
        this.editParameterPage = new EditParameterPage(driver);
    }

    protected List<String> listPaginationNumbers(){
        List<String> listPaginationNumbers= new ArrayList<String>();
        WebElement webElementPagination = editParameterPage.getRowPaginationParameterPage();
        String[] elementsPagination = webElementPagination.getText()
                .replace(" ", "-").split("-");
        for (String elementPagination : elementsPagination ){
            if(StringUtils.isNumeric(elementPagination)){
                listPaginationNumbers.add(elementPagination);
            }
        }
        return listPaginationNumbers;
    }

    protected boolean isvalidSession(){
        String templateCodeSession = editParameterPage.getTemplateCodeSesion();
        if( !templateCodeSession.contains(TEMPLATE_CODE)){
            LOGGER.error("ERROR, Codigo de Plantilla en session invalido, " +
                    "<" + templateCodeSession + "> " +
                    ", TEMPLATE_CODE <"+TEMPLATE_CODE+">");
            return false;
        }
        return true;
    }


}
