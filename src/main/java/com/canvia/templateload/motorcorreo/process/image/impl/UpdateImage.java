package com.canvia.templateload.motorcorreo.process.image.impl;

import com.canvia.templateload.bean.ImageBean;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class UpdateImage extends ProcessImage{

    private static final Logger LOGGER = LoggerFactory.getLogger(UpdateImage.class);

    public UpdateImage(WebDriver driver, String templateCode, ImageBean imageBean) {
        super(driver, templateCode, imageBean);
    }

    public boolean action() {
        LOGGER.info("...invoke UpdateImage.action ...");

        String pathImage = pathImageLoad(imageBean.getName());
        if( "".equals(pathImage)){
            return false;
        }

        List<String> listPaginationNumbers = listPaginationNumbers();
        for ( String paginationNumber:listPaginationNumbers ) {
             editImagePage.gotoImagenPage(paginationNumber);
             List<WebElement> imagesRow= editImagePage.getRowsTableImagenPage();

             int rowNumber = 0;
            for (WebElement imageRow: imagesRow) {
                if(imageRow.getText().contains(NO_RECORD_FOUND)){
                    LOGGER.error("Plantilla no tiene imagenes cargadas, " + NO_RECORD_FOUND);
                    return false;
                }
                if( imageRow.getText().contains(imageBean.getCode()) ){
                    if( !isvalidSession()){
                        return false;
                    }
                    editImagePage.selectOptionEditImage(rowNumber);
                    editImagePage.loadImage(pathImage);
                    editImagePage.selectButtonSave();
                    return true;
                }
                rowNumber++;
            }
        }
        LOGGER.error("Imagenen a Modificar no existe en WAS!!!");
        return false;
    }


}
