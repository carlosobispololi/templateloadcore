package com.canvia.templateload.motorcorreo.process.image;

import com.canvia.templateload.bean.ImageBean;
import com.canvia.templateload.motorcorreo.process.image.impl.CreateImage;
import com.canvia.templateload.motorcorreo.process.image.impl.DeleteImage;
import com.canvia.templateload.motorcorreo.process.image.impl.UpdateImage;
import org.openqa.selenium.WebDriver;

public class FactoryImage {

    private static final String MODIFICAR_ACTION ="MODIFICAR";
    private static final String DELETE_ACTION ="ELIMINAR";
    private static final String CREATE_ACTION ="NUEVO";

    public static ImageAction getImageAction(WebDriver driver,String templateCode ,ImageBean imageBean){

        String action = imageBean.getAction().toUpperCase();
        if(MODIFICAR_ACTION.equals(action)){
            return new UpdateImage(driver,templateCode,imageBean);
        }
        else if(DELETE_ACTION.equals(action)){
            return new DeleteImage(driver,templateCode,imageBean);
        }
        else if(CREATE_ACTION.equals(action)){
            return new CreateImage(driver,templateCode,imageBean);
        }
        else{
            throw new IllegalArgumentException("Action no defined"
                    + String.format(" <%s>", action));
        }
    }
}
