package com.canvia.templateload.motorcorreo.process.parameter.impl;

import com.canvia.templateload.bean.ParameterBean;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class DeleteParameter extends ProcessParameter{

    private static final Logger LOGGER = LoggerFactory.getLogger(DeleteParameter.class);

    public DeleteParameter(WebDriver driver, String codeTemplate, ParameterBean parameterBean) {
        super(driver, codeTemplate, parameterBean);
    }

    @Override
    public boolean action() {
        LOGGER.info("...invoke DeleteParameter.action ...");

        List<String> listPaginationNumbers = listPaginationNumbers();
        for ( String paginationNumber:listPaginationNumbers ) {
            editParameterPage.gotoParameterPage(paginationNumber);
            List<WebElement> parametersRow= editParameterPage.getRowsTableParameterPage();

            int rowNumber = 0;
            for (WebElement parameterRow: parametersRow) {
                if(parameterRow.getText().contains(NO_RECORD_FOUND)){
                    LOGGER.error("Plantilla no tiene parametros registrados, " + NO_RECORD_FOUND);
                    return false;
                }
                if( parameterRow.getText().contains(parameterBean.getFieldCode()) ){
                    if( !isvalidSession()){
                        return false;
                    }
                    editParameterPage.selectOptionDeleteParameter(rowNumber);
                    editParameterPage.confirmDelete();
                    return true;
                }
                rowNumber++;
            }
        }
        LOGGER.error("Parametro a Modificar no existe en WAS!!!");
        return false;
    }
}
