package com.canvia.templateload.dao.impl;

import com.canvia.templateload.bean.TemplateBean;
import com.canvia.templateload.dao.ITemplateDao;
import com.canvia.templateload.mapper.Mapper;
import com.canvia.templateload.mapper.impl.TemplateMapper;
import com.canvia.templateload.util.ExcelUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;

public class TemplateExcelDaoImpl implements ITemplateDao {

    private static final Logger LOGGER=  LoggerFactory.getLogger(TemplateExcelDaoImpl.class);

    Mapper<ExcelUtil, TemplateBean> templateBeanMapper;

    public TemplateExcelDaoImpl(){
        templateBeanMapper = new TemplateMapper();
    }

    public TemplateBean buildTemplateBean(InputStream inputStream) throws  Exception{
        LOGGER.info("...invoke TemplateExcelDaoImpl.buildTemplateBean ...");
        ExcelUtil excelUtil = ExcelUtil.getINSTANCE(inputStream);
        return templateBeanMapper.map(excelUtil);
    }


}
