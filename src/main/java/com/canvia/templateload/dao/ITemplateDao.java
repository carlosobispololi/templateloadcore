package com.canvia.templateload.dao;

import com.canvia.templateload.bean.TemplateBean;

import java.io.InputStream;

public interface ITemplateDao {

    TemplateBean buildTemplateBean(InputStream inputStream) throws  Exception;
}
