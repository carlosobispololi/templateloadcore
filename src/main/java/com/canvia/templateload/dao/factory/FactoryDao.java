package com.canvia.templateload.dao.factory;

import com.canvia.templateload.dao.ITemplateDao;

public abstract class FactoryDao {

    public static final int EXCEL_FACTORY = 1;

    public abstract ITemplateDao getPlantillaDao();

    public static FactoryDao getFactory(int factory){

        switch (factory){
            case EXCEL_FACTORY:
                return new ExcelFactoryDao();
            default:
                throw new IllegalArgumentException();
        }
    }
}
