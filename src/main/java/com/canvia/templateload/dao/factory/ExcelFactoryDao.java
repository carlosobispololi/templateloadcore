package com.canvia.templateload.dao.factory;

import com.canvia.templateload.dao.ITemplateDao;
import com.canvia.templateload.dao.impl.TemplateExcelDaoImpl;

public class ExcelFactoryDao extends FactoryDao{

    public ITemplateDao getPlantillaDao() {
        return new TemplateExcelDaoImpl();
    }
}
