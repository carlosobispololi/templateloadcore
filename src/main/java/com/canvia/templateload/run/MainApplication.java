package com.canvia.templateload.run;

import com.canvia.templateload.facade.ITemplateLoadFacade;
import com.canvia.templateload.facade.impl.TemplateLoadExcelFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * created on 21/05/2019
 *
 * @autor Canvia
 * */
public class MainApplication {

    private static final Logger LOGGER = LoggerFactory.getLogger(MainApplication.class);

    public static void main(String[] args) {
        long identificator = System.currentTimeMillis();
        LOGGER.info("==================== START TEMPLATE LOAD ( ID: " +
                identificator+" ) ==================== ");
        ITemplateLoadFacade templateLoadFacade = new TemplateLoadExcelFacade();
        templateLoadFacade.processFile();
        LOGGER.info("==================== END TEMPLATE LOAD ( ID: " +
                identificator+" ) ==================== ");

    }
}
