package com.canvia.templateload.bean;

import java.util.List;

public class TemplateBean{

    private String code;
    private Integer pageNumber;
    private List<ParameterBean> parameters;
    private List<ImageBean> images;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(Integer pageNumber) {
        this.pageNumber = pageNumber;
    }

    public List<ParameterBean> getParameters() {
        return parameters;
    }

    public void setParameters(List<ParameterBean> parameters) {
        this.parameters = parameters;
    }

    public List<ImageBean> getImages() {
        return images;
    }

    public void setImages(List<ImageBean> images) {
        this.images = images;
    }

    @Override
    public String toString() {
        return "TemplateBean{" +
                "code='" + code + '\'' +
                ", pageNumber=" + pageNumber +
                '}';
    }
}
