package com.canvia.templateload.bean;

public class ParameterBean {

    private String fieldCode;
    private String sourceCode;
    private String description;
    private String indTable;
    private Integer postInitial;
    private Integer length;
    private Integer numberColumns;
    private Integer numberRows;
    private String typeField;
    private String mantEspaces;
    private String action;

    public String getFieldCode() {
        return fieldCode;
    }

    public void setFieldCode(String fieldCode) {
        this.fieldCode = fieldCode;
    }

    public String getSourceCode() {
        return sourceCode;
    }

    public void setSourceCode(String sourceCode) {
        this.sourceCode = sourceCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIndTable() {
        return indTable;
    }

    public void setIndTable(String indTable) {
        this.indTable = indTable;
    }

    public Integer getPostInitial() {
        return postInitial;
    }

    public void setPostInitial(Integer postInitial) {
        this.postInitial = postInitial;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public Integer getNumberColumns() {
        return numberColumns;
    }

    public void setNumberColumns(Integer numberColumns) {
        this.numberColumns = numberColumns;
    }

    public Integer getNumberRows() {
        return numberRows;
    }

    public void setNumberRows(Integer numberRows) {
        this.numberRows = numberRows;
    }

    public String getTypeField() {
        return typeField;
    }

    public void setTypeField(String typeField) {
        this.typeField = typeField;
    }

    public String getMantEspaces() {
        return mantEspaces;
    }

    public void setMantEspaces(String mantEspaces) {
        this.mantEspaces = mantEspaces;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    @Override
    public String toString() {
        return "ParameterBean{" +
                "fieldCode='" + fieldCode + '\'' +
                ", SourceCode='" + sourceCode + '\'' +
                ", description='" + description + '\'' +
                ", indTable='" + indTable + '\'' +
                ", postInitial=" + postInitial +
                ", length=" + length +
                ", numberColumns=" + numberColumns +
                ", numberRows=" + numberRows +
                ", typeField='" + typeField + '\'' +
                ", mantEspaces='" + mantEspaces + '\'' +
                ", action='" + action + '\'' +
                '}';
    }
}
