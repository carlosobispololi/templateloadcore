package com.canvia.templateload.constant;

import com.canvia.templateload.util.bundle.ResourceBundleUtil;

public interface ApplicationConstant {


    String CONFIG_RESOURCE_BUNDLE = "config";
    String CONFIG_RESOURCE_EXTERNAL_BUNDLE = "./conf/config.properties";

    String URL_BASE = ResourceBundleUtil.getProperty("url.base");
    String LOGIN_PAGE =  URL_BASE + "/login.htm";
    String URL_TEMPLATE_PAGINATION_PAGE = URL_BASE + "/manttoPlantilla.htm?actionLink=table-controlLink&page=";
    String URL_IMAGE_PAGINATION_PAGE = URL_BASE + "/formatoPlantilla.htm?actionLink=table-controlLink&page=";
    String URL_PARAMETER_PAGINATION_PAGE = URL_BASE + "/tipoPlantilla.htm?actionLink=tablaParametro-controlLink&page=";

    String PATH_CHROME_DRIVE = ResourceBundleUtil.getProperty("path.chrome.drive");

    String USER = ResourceBundleUtil.getProperty("user");
    String PASSWORD = ResourceBundleUtil.getProperty("password");

    String PATH_PROCESS = ResourceBundleUtil.getProperty("path.process");

}
