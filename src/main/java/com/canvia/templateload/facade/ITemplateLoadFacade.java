package com.canvia.templateload.facade;

public interface ITemplateLoadFacade {

    void processFile();
}
