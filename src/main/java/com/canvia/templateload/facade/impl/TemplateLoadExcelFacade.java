package com.canvia.templateload.facade.impl;

import com.canvia.templateload.bean.TemplateBean;
import com.canvia.templateload.facade.ITemplateLoadFacade;
import com.canvia.templateload.motorcorreo.NewBeginningProcess;
import com.canvia.templateload.service.ITemplateService;
import com.canvia.templateload.service.impl.TemplateServiceImpl;
import com.canvia.templateload.util.bundle.ResourceBundleUtil;
import com.canvia.templateload.validator.Validator;
import com.canvia.templateload.validator.impl.TemplateValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TemplateLoadExcelFacade implements ITemplateLoadFacade {

    private static final Logger LOGGER = LoggerFactory.getLogger(TemplateLoadExcelFacade.class);
    private static final String PATH_FILE_PROPERTY = "path_file";

    ITemplateService templateService;
    Validator<TemplateBean> templateBeanValidator;

    public TemplateLoadExcelFacade(){
        templateService = new TemplateServiceImpl();
        templateBeanValidator = new TemplateValidator();
    }

    public void processFile() {
        LOGGER.info("...invoke TemplateLoadExcelFacade.processFile ...");
        try {
            String pathExcelTemplate= ResourceBundleUtil.getProperty( PATH_FILE_PROPERTY );
            TemplateBean templateBean= templateService.getTemplateBean(pathExcelTemplate);
            if( !templateBeanValidator.validate(templateBean)){
                throw new Exception("ERROR validation templateBean");
            }
            NewBeginningProcess newBeginningProcess = new NewBeginningProcess();
            newBeginningProcess.processImages(templateBean);
            newBeginningProcess.processParameters(templateBean);
        }catch (Exception e){
            LOGGER.error("Error General ", e);
        }

        LOGGER.info("...FIN TemplateLoadExcelFacade.processFile ...");
    }



}
