package com.canvia.templateload.mapper;

public interface Mapper<S, T> {

    T map(S input);

}
