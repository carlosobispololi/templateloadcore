package com.canvia.templateload.mapper.impl;

import com.canvia.templateload.bean.ImageBean;
import com.canvia.templateload.bean.ParameterBean;
import com.canvia.templateload.bean.TemplateBean;
import com.canvia.templateload.mapper.Mapper;
import com.canvia.templateload.util.ExcelUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class TemplateMapper implements Mapper<ExcelUtil, TemplateBean> {

    private static final Logger LOGGER= LoggerFactory.getLogger(TemplateMapper.class);
    private static final String CELL_CODE ="D2";
    private static final String CELL_PAGENUMBER = "D3";
    private Mapper<ExcelUtil, List<ParameterBean>> parametersMapper;
    private Mapper<ExcelUtil, List<ImageBean>> imagesMapper;

    public TemplateMapper(){
        parametersMapper = new ParametersMapper();
        imagesMapper = new ImagensMapper();
    }

    public TemplateBean map(ExcelUtil input) {
        LOGGER.info("...invoke TemplateMapper.map ...");
        if(input==null){
            return null;
        }
        TemplateBean templateBean= new TemplateBean();
        templateBean.setCode( input.getStringCellValue(CELL_CODE));
        templateBean.setPageNumber( new Double(input.getStringCellValue(CELL_PAGENUMBER)).intValue() );
        templateBean.setParameters( parametersMapper.map( input));
        templateBean.setImages( imagesMapper.map(input));
        LOGGER.debug("templateBean: " + templateBean.toString());
        return templateBean;
    }
}
