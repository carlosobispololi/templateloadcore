package com.canvia.templateload.mapper.impl;

import com.canvia.templateload.bean.ImageBean;
import com.canvia.templateload.mapper.Mapper;
import com.canvia.templateload.util.ExcelUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * created on 23/05/2019
 *
 * @author Canvia
 */
public class ImagensMapper implements Mapper<ExcelUtil, List<ImageBean>> {

    private static final Logger LOGGER= LoggerFactory.getLogger( ImagensMapper.class);

    private static final int INDICATOR_COLUMN = 1;
    private static final String INDICATOR_IMAGES = "IMAGENES";

    public List<ImageBean> map(ExcelUtil input) {
        if(input==null){
            return null;
        }
        List<ImageBean> imageBeans = new ArrayList<ImageBean>();
        ImageBean imageBean;
        Iterator<Row> rowIterator = input.getExcelRows();
        int numberRowImages = 0;
        boolean startProcess = false;
        while (rowIterator.hasNext()) {
            imageBean = new ImageBean();
            Row row = rowIterator.next();

            String indicator_column =row.getCell(INDICATOR_COLUMN).toString();

            if( INDICATOR_IMAGES.equals(indicator_column) ){
                // Se omite la cabecera
                numberRowImages = row.getRowNum() + 2;
            }
            if( numberRowImages == row.getRowNum() ){ startProcess = true; }

            if( startProcess ){
                if( "".equals(indicator_column)){ break; }
                Iterator<Cell> cellIterator = row.cellIterator();
                int column_number = 0;
                while (cellIterator.hasNext()) {
                    column_number++;
                    Cell cell = cellIterator.next();
                    mapImageBean(input, cell, column_number, imageBean);
                }
                LOGGER.info("Image find : " + imageBean.toString());
                imageBeans.add(imageBean);
            }

        }
        return imageBeans;
    }

    private void mapImageBean(ExcelUtil input, Cell cell,
                              int column_number, ImageBean  imageBean){
        if( column_number==1 ){
            imageBean.setCode( input.getCellValue(cell) );
        } else if( column_number==2 ){
            imageBean.setName( input.getCellValue(cell) );
        } else if( column_number==3 ){
            imageBean.setAction( input.getCellValue(cell) );
        }
    }
}
