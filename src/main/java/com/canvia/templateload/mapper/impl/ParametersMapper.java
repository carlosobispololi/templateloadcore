package com.canvia.templateload.mapper.impl;

import com.canvia.templateload.bean.ParameterBean;
import com.canvia.templateload.mapper.Mapper;
import com.canvia.templateload.util.ExcelUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * created on 23/05/2019
 *
 * @author Canvia
 */
public class ParametersMapper implements Mapper<ExcelUtil, List<ParameterBean>> {

    private static final Logger LOGGER= LoggerFactory.getLogger(ParametersMapper.class);
    private static final int INDICATOR_COLUMN = 1;
    private static final String INDICATOR_PARAMETERS = "PARAMETROS";

    public List<ParameterBean> map(ExcelUtil input) {
        LOGGER.info("...invoke ParametersMapper.map ...");
        if( input==null ){
            return null;
        }
        List<ParameterBean> parameterBeans = new ArrayList<ParameterBean>();
        ParameterBean parameterBean;
        Iterator<Row> rowIterator = input.getExcelRows();

        int numberRowParameters = 0;
        boolean startProcess = false;
        while (rowIterator.hasNext()) {
            parameterBean = new ParameterBean();
            Row row = rowIterator.next();

            String indicator_column =row.getCell(INDICATOR_COLUMN).toString();

            if( INDICATOR_PARAMETERS.equals(indicator_column) ){
                // Se omite la cabecera
                numberRowParameters = row.getRowNum() + 2;
            }
            if( numberRowParameters == row.getRowNum() ){ startProcess = true; }

            if( startProcess ){
                if( "".equals(indicator_column)){ break; }

                Iterator<Cell> cellIterator = row.cellIterator();
                int column_number = 0;
                while (cellIterator.hasNext()) {
                    column_number++;
                    Cell cell = cellIterator.next();
                    mapParameterBean(input, cell, column_number, parameterBean);
                }
                LOGGER.info("Parameter find : " + parameterBean.toString());
                parameterBeans.add(parameterBean);
            }
        }
        return parameterBeans;
    }

    private void mapParameterBean(ExcelUtil input, Cell cell,
                                  int column_number, ParameterBean parameterBean){

        if(column_number==1){
            parameterBean.setFieldCode( input.getCellValue(cell));
        } else if(column_number==2){
            parameterBean.setSourceCode(input.getCellValue(cell));
        } else if(column_number==3){
            parameterBean.setDescription(input.getCellValue(cell));
        } else if(column_number==4){
            parameterBean.setIndTable(input.getCellValue(cell));
        } else if(column_number==5){
            String posInitialValue =  input.getCellValue(cell);
            if(!"".equals(posInitialValue)){
                parameterBean.setPostInitial(new Double(posInitialValue).intValue() );
            }
        } else if(column_number==6){
            String length =  input.getCellValue(cell);
            if( !"".equals(length)) {
                parameterBean.setLength(new Double(length).intValue());
            }
        } else if(column_number==7){
            String numberColumns= input.getCellValue(cell);
            if( !"".equals(numberColumns)) {
                parameterBean.setNumberColumns(new Double(numberColumns).intValue());
            }
        } else if(column_number==8){
            String numberRows= input.getCellValue(cell);
            if( !"".equals(numberRows)) {
                parameterBean.setNumberRows(new Double(numberRows).intValue());
            }
        } else if(column_number==9){
            parameterBean.setTypeField(input.getCellValue(cell));
        } else if(column_number==10){
            parameterBean.setMantEspaces(input.getCellValue(cell));
        } else if(column_number==11){
            parameterBean.setAction(input.getCellValue(cell));
        }
    }
}
